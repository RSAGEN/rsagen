<div align="center">

<img src="resources/rsagen.png"><br>

<b>RSAGEN</b> is a Qt application that uses GNU MP to generate and use RSA keypairs.

</div>

---

## Compiling

The compilation is made very easy with the help of the Qt Makefile generator `qmake`.

First we need the dependencies:

#### Debian

```
# apt install g++ make qt4-qmake libgmp-dev libqt4-dev
```

#### TODO other GNU/Linux distribution PM commands

Then we can start downloading, compiling and installing the software:

```bash
git clone https://gitlab.com/Capuno/rsagen.git
cd rsagen/build
# Optional: Disable LEET_UI in rsagen.pro
qmake
make
sudo make install
```

To uninstall the software from your system:

```bash
 # Inside the build folder in the project
 sudo make uninstall
```

## Usage

The usage is very simple and only requires a little understanding about the RSA algorithm. After install, run the program with `rsagen`.

<div align="center">

<h3>Generating a randomized key pair</h3>

We can generate random key with N bits, first, select the <code>Key</code> Menu and then the <code>Set Bits...</code>
, this will open a new window in which you can select the number of bits used in the key pairs. <br>

<img src="https://rsagen.capuno.cat/img/setbits.gif"><br>

After that, we can randomize a key pair with that number of bits from <code>Key</code> > <code>Randomize Key Pairs</code><br>

<img src="https://rsagen.capuno.cat/img/randomize.gif">

</div>

---

Copyright &copy; 2018 Jaume Fuster i Claris

**RSAGEN** is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

More information inside the LICENSE file or at https://www.gnu.org/licenses/gpl.html.
