/*  interface.cpp - Definition of the Qt user interface main components.

    RSAGEN - Qt application to generate, load and save RSA keys and
    encode/decode strings with the RSA encryption.

    Copyright (C) 2018 Jaume Fuster i Claris

    This file is part of RSAGEN.

    RSAGEN is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RSAGEN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RSAGEN inside the file "LICENSE".
    If not, see <https://www.gnu.org/licenses/gpl.html>.

*/


#include "interface.hpp"


void rsa_window::loadpu() {
    QString fileName = QFileDialog::getOpenFileName(this, tr("Load Public Key"), "", tr("RSAGEN Public Keys (*.pubkey);;All Files (*)"));
    QString Nb, Eb;
    if (!fileName.isEmpty()) {
        QFile file(fileName);
        if (!file.open(QIODevice::ReadOnly)) {
            QMessageBox::information(this, tr("Unable to read file!"), file.errorString());
        } else {
            QTextStream in(&file);
            while (!in.atEnd()) {
                QString num = in.readLine();
                num.replace(" ", "");
                if (num.at(0) == 'N') {
                    Nb = num.mid(2);
                } else if (num.at(0) == 'E') {
                    Eb = num.mid(2);
                }
            }
        }
        QRegExp numcheck("\\d*");
        if (!Nb.isEmpty() && !Eb.isEmpty() && numcheck.exactMatch(Nb) && numcheck.exactMatch(Eb)) {
            std::string Ns = Nb.toUtf8().constData();
            std::string Es = Eb.toUtf8().constData();
            this->rsae.setPublicKey(Ns, Es);
            std::string nb = mpz_get_str(NULL, 10, this->rsae.public_key.n);
            nb = nb.length() > MAX_KEY_LEN ? nb.substr(0, MAX_KEY_LEN) + "..." : nb;
            std::string eb = mpz_get_str(NULL, 10, this->rsae.public_key.e);
            eb = eb.length() > MAX_KEY_LEN ? eb.substr(0, MAX_KEY_LEN) + "..." : eb;
            std::string pukb = (std::string)"Public Key:\n\n  N: " + nb + "\n  E: " + eb;
            this->puk->setText(pukb.c_str());
            this->txtio->setReadOnly(false);
            this->txtio->style()->unpolish(this->txtio);
            this->txtio->style()->polish(this->txtio);
            this->savepuQA->setEnabled(true);
            this->unloadQA->setEnabled(true);
        } else {
            QMessageBox::information(this, tr("Unable to load public key!"), tr("They key you tried to load is invalid."));
        }
    }
}
void rsa_window::savepu() {
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save Public Key"), "rsagen.pubkey", tr("RSAGEN Public Keys (*.pubkey);;All Files (*)"));
    if (!fileName.isEmpty()) {
        QFile file(fileName);
        if (!file.open(QIODevice::WriteOnly)) {
            QMessageBox::information(this, tr("Unable to write file!"), file.errorString());
        } else {
            char *nb = mpz_get_str(NULL, 10, this->rsae.public_key.n);
            char *eb = mpz_get_str(NULL, 10, this->rsae.public_key.e);
            // Plaintext to manually modify it if wanted
            // TODO research about Base64-encoded RSA key pairs
            std::string pukb = (std::string)"N: " + nb + "\nE: " + eb + "\n";
            file.write(pukb.c_str(), qstrlen(pukb.c_str()));
        }
    }
}
void rsa_window::loadpr() {
    QString fileName = QFileDialog::getOpenFileName(this, tr("Load Private Key"), "", tr("RSAGEN Private Keys (*.prikey);;All Files (*)"));
    QString Nb, Db;
    if (!fileName.isEmpty()) {
        QFile file(fileName);
        if (!file.open(QIODevice::ReadOnly)) {
            QMessageBox::information(this, tr("Unable to read file!"), file.errorString());
        } else {
            QTextStream in(&file);
            while (!in.atEnd()) {
                QString num = in.readLine();
                num.replace(" ", "");
                if (num.at(0) == 'N') {
                    Nb = num.mid(2);
                } else if (num.at(0) == 'D') {
                    Db = num.mid(2);
                }
            }
        }
        QRegExp numcheck("\\d*");
        if (!Nb.isEmpty() && !Db.isEmpty() && numcheck.exactMatch(Nb) && numcheck.exactMatch(Db)) {
            std::string Ns = Nb.toUtf8().constData();
            std::string Ds = Db.toUtf8().constData();
            this->rsae.setPrivateKey(Ns, Ds);
            std::string nb = mpz_get_str(NULL, 10, this->rsae.private_key.n);
            nb = nb.length() > MAX_KEY_LEN ? nb.substr(0, MAX_KEY_LEN) + "..." : nb;
            std::string db = mpz_get_str(NULL, 10, this->rsae.private_key.d);
            db = db.length() > MAX_KEY_LEN ? db.substr(0, MAX_KEY_LEN) + "..." : db;
            std::string pukb = (std::string)"Private Key:\n\n  N: " + nb + "\n  D: " + db;
            this->prk->setText(pukb.c_str());
            this->rsaio->setReadOnly(false);
            this->rsaio->style()->unpolish(this->rsaio);
            this->rsaio->style()->polish(this->rsaio);
            this->saveprQA->setEnabled(true);
            this->unloadQA->setEnabled(true);
        } else {
            QMessageBox::information(this, tr("Unable to load private key!"), tr("They key you tried to load is invalid."));
        }
    }
}
void rsa_window::savepr() {
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save Private Key"), "rsagen.prikey", tr("RSAGEN Private Keys (*.prikey);;All Files (*)"));
    if (!fileName.isEmpty()) {
        QFile file(fileName);
        if (!file.open(QIODevice::WriteOnly)) {
            QMessageBox::information(this, tr("Unable to write file!"), file.errorString());
        } else {
            char *nb = mpz_get_str(NULL, 10, this->rsae.private_key.n);
            char *db = mpz_get_str(NULL, 10, this->rsae.private_key.d);
            std::string pukb = (std::string)"N: " + nb + "\nD: " + db + "\n";
            file.write(pukb.c_str(), qstrlen(pukb.c_str()));
        }
    }
}
void rsa_window::unload() {
    this->rsae.clear();
    this->puk->setText("Public Key:\n\n  N: [Not loaded]\n  E: [Not loaded]");
    this->prk->setText("Private Key:\n\n  N: [Not loaded]\n  D: [Not loaded]");

    this->txtio->clear();
    this->txtio->setReadOnly(true);
    this->txtio->style()->unpolish(this->txtio);
    this->txtio->style()->polish(this->txtio);

    this->rsaio->clear();
    this->rsaio->setReadOnly(true);
    this->rsaio->style()->unpolish(this->rsaio);
    this->rsaio->style()->polish(this->rsaio);

    this->savepuQA->setEnabled(false);
    this->saveprQA->setEnabled(false);
    this->unloadQA->setEnabled(false);
}

void rsa_window::randomize() {
    std::string nb, eb, db;
    this->rsae.randomize(this->rsabits, this->seed);
    nb = mpz_get_str(NULL, 10, this->rsae.public_key.n);
    nb = nb.length() > MAX_KEY_LEN ? nb.substr(0, MAX_KEY_LEN) + "..." : nb;
    eb = mpz_get_str(NULL, 10, this->rsae.public_key.e);
    eb = eb.length() > MAX_KEY_LEN ? eb.substr(0, MAX_KEY_LEN) + "..." : eb;
    db = mpz_get_str(NULL, 10, this->rsae.private_key.d);
    db = db.length() > MAX_KEY_LEN ? db.substr(0, MAX_KEY_LEN) + "..." : db;
    std::string pukb = (std::string)"Public Key:\n\n  N: " + nb + "\n  E: " + eb;
    std::string prkb = (std::string)"Private Key:\n\n  N: " + nb + "\n  D: " + db;
    this->puk->setText(pukb.c_str());
    this->prk->setText(prkb.c_str());
    this->txtio->setReadOnly(false);
    this->txtio->style()->unpolish(this->txtio);
    this->txtio->style()->polish(this->txtio);
    this->rsaio->setReadOnly(false);
    this->rsaio->style()->unpolish(this->rsaio);
    this->rsaio->style()->polish(this->rsaio);
    this->updateio(TXT_EVENT);
    this->savepuQA->setEnabled(true);
    this->saveprQA->setEnabled(true);
    this->unloadQA->setEnabled(true);
}
void rsa_window::setbits() {
    this->rsabits = QInputDialog::getInt(0, "RSAGEN - Set Bits", "Set RSA bits to:", this->rsabits, 6);
}
void rsa_window::setseed() {
    QByteArray test = QInputDialog::getText(0, "RSAGEN - Set Seed", "Set random seed to:").toLocal8Bit();
    for (int i = 0; i < test.length(); ++i) {
        this->seed = (this->seed+(unsigned int)test.at(i)*(unsigned int)test.at(i)) % UINT_MAX;
    }
}

void rsa_window::about() {
    system("xdg-open " HELP_ABOUT " &");
}
void rsa_window::license() {
    system("xdg-open " HELP_LICENSE " &");
}
void rsa_window::aboutrsa() {
    system("xdg-open " HELP_RSA " &");
}

void rsa_window::updateio(uint8_t id) { //overkill?
    QString rsao = rsaio->toPlainText();
    QString txto = txtio->toPlainText();
    switch (id) {
        case TXT_EVENT:
            if (this->rsae.public_key.loaded) {
                this->rsaio->setText(QString::fromStdString(txto.isEmpty() ? "" : rsaEncrypt(this->rsae.public_key, txto.toUtf8().constData())));
            }
            break;
        case RSA_EVENT:
            if (this->rsae.private_key.loaded) {
                this->txtio->setText(QString::fromStdString(rsao.isEmpty() ? "" : rsaDecrypt(this->rsae.private_key, rsao.toUtf8().constData())));
            }
            break;
    }
}

rsa_window::rsa_window() {

    // GNU Coding Standards recommends to use if instead of the compiler macro ifdef. (3.5 Conditional Compilation)
    #ifdef LEET_UI

    qApp->setStyleSheet("* { background-color: #0F160F;color: #ccc; }\
        QTextEdit[readOnly=\"true\"] {background-color: #202420;border: none;}\
        QTextEdit[readOnly=\"false\"] {background-color: #182418;border: none;}\
        QMenuBar::item:selected {background-color: #182418;}\
        QMenu {border: 1px solid #182418;}\
        QMenu::separator {background-color: #182418; height: 1px;}\
        QMenu:selected {background-color: #182418; color: #ccc;}\
        QMenu:disabled {color: #666;}\
        QMenu:disabled:selected {background-color: #0F160F}\
        QPushButton {background-color: #182418;border: none;padding: 5px;}\
        QPushButton:hover {background-color: #1F381F;}\
        QLabel {font-family: monospace;}");

    #endif

    QAction *randomize = new QAction("&Randomize Key Pairs", this);
    this->unloadQA = new QAction("&Unload Key Pairs", this);
    QAction *setseed = new QAction("&Set Random Seed...", this);
    QAction *setbits = new QAction("&Set Key Bits...", this);
    QAction *quit = new QAction("&Quit", this);
    QAction *loadpu = new QAction("&Load Public Key...", this);
    this->savepuQA = new QAction("&Save Public Key...", this);
    QAction *loadpr = new QAction("&Load Private Key...", this);
    this->saveprQA = new QAction("&Save Private Key...", this);
    QAction *about = new QAction("&About", this);
    QAction *license = new QAction("&License", this);
    QAction *aboutrsa = new QAction("&About RSA", this);

    QMenu *file, *key, *help;
    file = menuBar()->addMenu("&File");
    file->addAction(loadpu);
    file->addAction(this->savepuQA);
    file->addSeparator();
    file->addAction(loadpr);
    file->addAction(this->saveprQA);
    file->addSeparator();
    file->addAction(quit);
    key = menuBar()->addMenu("&Key");
    key->addAction(randomize);
    key->addAction(this->unloadQA);
    key->addSeparator();
    key->addAction(setseed);
    key->addAction(setbits);
    help = menuBar()->addMenu("&Help");
    help->addAction(about);
    help->addAction(license);
    help->addSeparator();
    help->addAction(aboutrsa);

    this->savepuQA->setEnabled(false);
    this->saveprQA->setEnabled(false);
    this->unloadQA->setEnabled(false);

    puk = new QLabel(this);
    puk->setText("Public Key:\n\n  N: [Not loaded]\n  E: [Not loaded]");
    puk->setGeometry(30, 30, W / 2 - 60, 100);

    prk = new QLabel(this);
    prk->setText("Private Key:\n\n  N: [Not loaded]\n  D: [Not loaded]");
    prk->setGeometry(W / 2 + 30, 30, W / 2 - 60, 100);

    txtio = new QTextUpdate(this, TXT_EVENT);
    txtio->setTabChangesFocus(true);
    txtio->setGeometry(M, 130, W / 2 - (M + (M/2)), H - (130 + M));
    this->txtio->setReadOnly(true);

    rsaio = new QTextUpdate(this, RSA_EVENT);
    rsaio->setTabChangesFocus(true);
    rsaio->setGeometry(W / 2 + M / 2, 130, W / 2 - (M + (M/2)), H - (130 + M));
    this->rsaio->setReadOnly(true);

    connect(loadpu, SIGNAL(triggered()), this, SLOT(loadpu()));
    connect(savepuQA, SIGNAL(triggered()), this, SLOT(savepu()));
    connect(loadpr, SIGNAL(triggered()), this, SLOT(loadpr()));
    connect(saveprQA, SIGNAL(triggered()), this, SLOT(savepr()));
    connect(unloadQA, SIGNAL(triggered()), this, SLOT(unload()));
    connect(quit, SIGNAL(triggered()), qApp, SLOT(quit()));
    connect(randomize, SIGNAL(triggered()), this, SLOT(randomize()));
    connect(setseed, SIGNAL(triggered()), this, SLOT(setseed()));
    connect(setbits, SIGNAL(triggered()), this, SLOT(setbits()));
    connect(about, SIGNAL(triggered()), this, SLOT(about()));
    connect(license, SIGNAL(triggered()), this, SLOT(license()));
    connect(aboutrsa, SIGNAL(triggered()), this, SLOT(aboutrsa()));

}

void rsa_window::resizeEvent(QResizeEvent* event) {
    QMainWindow::resizeEvent(event);
    QSize sz = this->size();
    unsigned int Hd = sz.height();
    unsigned int Wd = sz.width();
    puk->setGeometry(30, 30, Wd / 2 - 60, 100);
    prk->setGeometry(sz.width() / 2 + 30, 30, Wd / 2 - 60, 100);
    txtio->setGeometry(M, 130, Wd / 2 - (M + (M/2)), Hd - (130 + M));
    rsaio->setGeometry(Wd / 2 + M / 2, 130, Wd / 2 - (M + (M/2)), Hd - (130 + M));
}


QTextUpdate::QTextUpdate(rsa_window * parent = 0, const uint8_t id = 0) : QTextEdit(parent){
    this->id = id;
    prwindow = qobject_cast<rsa_window*>(this->parent());
}

void QTextUpdate::keyPressEvent(QKeyEvent * ev) {
    if(ev->modifiers() & Qt::ControlModifier) {
        QTextEdit::keyPressEvent(ev);
    } else {
        switch (this->id) {
            case TXT_EVENT:
                QTextEdit::keyPressEvent(ev);
                break;
            case RSA_EVENT:
                if (!ev->text().contains(QRegExp("^[!-.:-~]$"))) {
                    QTextEdit::keyPressEvent(ev);
                }
                break;
            default:
                return;
        }
    }
    this->prwindow->updateio(id);
}
