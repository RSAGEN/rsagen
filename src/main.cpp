/*  main.cpp - Main function and QMainWindow (rsa_window) initialization.

    RSAGEN - Qt application to generate, load and save RSA keys and
    encode/decode strings with the RSA encryption.

    Copyright (C) 2018 Jaume Fuster i Claris

    This file is part of RSAGEN.

    RSAGEN is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RSAGEN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RSAGEN inside the file "LICENSE".
    If not, see <https://www.gnu.org/licenses/gpl.html>.

*/


#include <string>

#include "interface.hpp"


rsa_window * rwindow;

int main(int argc, char *argv[]) {
    QApplication app(argc, argv);
    rwindow = new rsa_window();
    rwindow->resize(W, H);
    rwindow->setFixedSize(QSize(W, H));
    rwindow->setWindowTitle("RSAGEN");
    rwindow->show();
    int ec = app.exec();
    delete rwindow;
    return ec;
}
