/*  rsa_engine.cpp - Definition of the RSA engine using GNU Multiple Precision Arithmetic Library (GMP).
 *  This file was a standalone before I added it to RSAGEN and can work as such if desired alongside rsa_engine.hpp.

    RSAGEN - Qt application to generate, load and save RSA keys and
    encode/decode strings with the RSA encryption.

    Copyright (C) 2018 Jaume Fuster i Claris

    This file is part of RSAGEN.

    RSAGEN is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RSAGEN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RSAGEN inside the file "LICENSE".
    If not, see <https://www.gnu.org/licenses/gpl.html>.

*/


#include "rsa_engine.hpp"


void rsa_c::randomPrime(mpz_t rpg, unsigned int bits) {
    mpz_t rng, maxV;
    mpz_init(rpg);
    char strRng[bits+1] = { '\0' };
    char maxRng[bits+1] = { '\0' };
    strRng[0] = '1';
    maxRng[0] = '1';
    for ( unsigned int i = 1; i < bits; ++i ) {
        char b = rand() % 2 == 0 ? '0' : '1';
        strRng[i] = b;
        maxRng[i] = '1';
    }
    mpz_init_set_str(rng, strRng, 2);
    mpz_init_set_str(maxV, maxRng, 2);
    mpz_nextprime(rpg, rng);
    if ( mpz_cmp(rpg, maxV) > 0 ) {
        randomPrime(rpg, bits);
    }
}

rsa_c::rsa_c() {
    mpz_inits(this->public_key.n, this->public_key.e,
                this->private_key.n, this->private_key.d, NULL);
}

void rsa_c::randomize(unsigned int bits, unsigned int seed) {

    mpz_t p, q, n, m, e, d;


    srand((seed+time(NULL)) % UINT_MAX);
    gmp_randstate_t state;
    gmp_randinit_mt(state);
    gmp_randseed_ui(state, (seed+time(NULL)) % UINT_MAX);

    // P
    randomPrime(p, bits/2);
    // Q
    do {
        randomPrime(q, bits/2);
    } while ( mpz_cmp(p, q) == 0 );
    // N
    mpz_init(n);
    mpz_mul(n, p, q);
    // M (Phi)
    mpz_init(m);
    mpz_t buffP, buffQ;
    mpz_inits(buffP, buffQ, NULL);
    mpz_sub_ui(buffP, p, 1);
    mpz_sub_ui(buffQ, q, 1);
    mpz_mul(m, buffP, buffQ);
    mpz_clears(buffP, buffQ, NULL);
    // E
    mpz_init(e);
    mpz_t gcd, buffM;
    mpz_inits(gcd, buffM, NULL);
    mpz_sub_ui(buffM, m, 4);
    do {
        mpz_urandomm(e, state, buffM);
        mpz_add_ui(e, e, 3);
        mpz_gcd(gcd, e, m);
    } while ( mpz_cmp_ui(gcd, 1) != 0 );
    mpz_clears(gcd, buffM, NULL);
    // D
    mpz_init(d);
    mpz_invert(d, e, m);

    mpz_init_set(this->public_key.n, n);
    mpz_init_set(this->public_key.e, e);
    mpz_init_set(this->private_key.n, n);
    mpz_init_set(this->private_key.d, d);

    mpz_clears(p, q, n, m, e, d, NULL);

    gmp_randclear(state);

    this->public_key.loaded = true;
    this->private_key.loaded = true;

}

void rsa_c::setPublicKey(std::string N, std::string E) {
    mpz_set_str(this->public_key.n, N.c_str(), 10);
    mpz_set_str(this->public_key.e, E.c_str(), 10);
    this->public_key.loaded = true;
}

void rsa_c::setPrivateKey(std::string N, std::string D) {
    mpz_set_str(this->private_key.n, N.c_str(), 10);
    mpz_set_str(this->private_key.d, D.c_str(), 10);
    this->private_key.loaded = true;
}

void rsa_c::clear() {
    mpz_clears(this->public_key.n, this->public_key.e,
                this->private_key.n, this->private_key.d, NULL);
    mpz_inits(this->public_key.n, this->public_key.e,
                this->private_key.n, this->private_key.d, NULL);
    private_key.loaded = false;
    public_key.loaded = false;
}

std::string rsaEncrypt(rsa_c::public_key_s pk, std::string str) {
    std::string output = "";
    mpz_t num, enc;
    mpz_init_set_ui(enc, 0);
    mpz_init_set_ui(num, 0);
    for (int i = str.length() - 1; i >= 0; i--) {
        if ((int)str[i] < 127 && (int)str[i] > 31) { // Ascii only, tmprly hardcoded
            mpz_mul_ui(num, num, 128);
            mpz_add_ui(num, num, (int)str[i]);
            if (mpz_cmp(pk.n, num) < 0) {
                mpz_sub_ui(num, num, (int)str[i++]);
                mpz_div_ui(num, num, 128);
                mpz_powm(enc, num, pk.e, pk.n);
                output.append((std::string)mpz_get_str(NULL, 10, enc) + " ");
                mpz_set_ui(num, 0);
                mpz_set_ui(enc, 0);
            }
        }
    }
    if (mpz_cmp_ui(num, 0) != 0) {
        mpz_powm(enc, num, pk.e, pk.n);
        output.append((std::string)mpz_get_str(NULL, 10, enc));
    }
    mpz_clears(num, enc, NULL);
    return output;
}

std::string rsaDecrypt(rsa_c::private_key_s pk, std::string num) {
    std::string output;
    size_t numsep = num.find_first_of(' ');
    if (numsep != std::string::npos) {
        num.append(" ");
        while (numsep != std::string::npos) {
            output.insert(0, rsaDecrypt(pk, num.substr(0, numsep)));
            num.erase(0, numsep+1);
            numsep = num.find_first_of(' ');
        }
    } else {
        char c;
        mpz_t preDec, dec, cBuff;
        mpz_init(dec);
        mpz_init_set_str(preDec, num.c_str(), 10);
        mpz_powm(dec, preDec, pk.d, pk.n);
        mpz_init_set_ui(cBuff, 1);
        unsigned long len = 1;
        while (mpz_cmp(dec, cBuff) > 0) {
            mpz_mul_ui(cBuff, cBuff, 128);
            len++;
        }
        for (unsigned int i = 0; i < len; ++i) {
            mpz_mod_ui(cBuff, dec, 128);
            mpz_sub(dec, dec, cBuff);
            mpz_tdiv_q_ui(dec, dec, 128);
            c = mpz_get_ui(cBuff);
            // Ignore null characters
            if (c != 0) output = output + c;
        }
        mpz_clears(preDec, dec, cBuff, NULL);
    }
    return output;
}
