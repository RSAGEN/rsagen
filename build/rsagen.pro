
TEMPLATE = app
TARGET = rsagen
DEPENDPATH += ../src
INCLUDEPATH += ../src
LIBS += -lgmp

QMAKE_CXXFLAGS += -lgmp
QMAKE_CXXFLAGS_WARN_OFF -= -Wunused-parameter -Wunused-function

HEADERS += ../src/rsa_engine.hpp ../src/interface.hpp
SOURCES += ../src/rsa_engine.cpp ../src/interface.cpp ../src/main.cpp

DEFINES += LEET_UI

# Thanks to Jesús Torres for his Medium post about make install with qmake, very helpful!

isEmpty(PREFIX) {
    PREFIX = /usr/local
}

BINDIR  = $$PREFIX/bin
DATADIR = $$PREFIX/share
#MANDIR = $$PREFIX/man/man1

INSTALLS += target #mandir desktop icon32

target.path = $$BINDIR

#mandir.path = $$MANDIR
#mandir.files = ../resources/rsagen.1

#desktop.path = $$DATADIR/applications
#desktop.files += $${TARGET}.desktop

#icon32.path = $$DATADIR/icons/hicolor/32x32/apps
#icon32.files += ../resources/$${TARGET}.png

QMAKE_DEL_DIR = rmdir --ignore-fail-on-non-empty # Sadly this flag is not defined by POSIX and only works in some distros
