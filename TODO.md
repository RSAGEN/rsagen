## APPLICATION
* ~~**IMPORTANT** Add OAEP~~ Changed for custom padding.
* **IMPORTANT** Accept unicode
* Comply with PKCS ? I mean maybe it's safer to not comply with it, always keep in mind the reason of this project, to keep safe people from an overestimated enemy like an authoritarian regime
* Is compatibility more important than some time added in case of compromised private keys?
* List of standards: https://en.wikipedia.org/wiki/PKCS
* PKCS #1 (2.2) https://tools.ietf.org/html/rfc8017
* Add more Key QMenu slots (vanity randomize, set padding method, etc)
* Start manpage > interface
* ~~Auto split for larger text~~
* Copy button for inputs
* ~~Tab to switch input~~
* ~~Save disabled when no key loaded~~
* **DISCARDED** ~~If loading key pair from already loaded, deload other key pair.~~

## CODE
* Merge load, save key functions interface.cpp (SLOT Lambda?)
* ~~Comment everything inside headers~~
* mpz_t to mpz_class (recompiling gmp with --enable-cxx?) is it even worth it?
* key pairs as pointers (rsa_c::(public|private)\_key_s)

## BUGS
* ~~When cursor changes update io, null characters get inside txtio, remove null characters entirely but the end ones.~~
* ~~std::string::insert causes SEGABRT~~

## CLI
* Add `--pubkey file` to load the public key from the command line.
* Add `--prikey file` to load the private key from the command line.

## (((((((((((UX)))))))))))
* Add a .desktop file in the project file
* ~~Add an icon for it ^~~
* ~~About website~~
* Website tutorials

## MISC
* Investigate why RSA key pairs are shared as base64